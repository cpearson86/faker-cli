<?php
const MAX_HISTORY_SIZE = 50;

require_once 'vendor/autoload.php';
$faker = Faker\Factory::create();
$fakerCompletion = new FakerCli\FakerCompletion($faker);

$historyFilePath = ($_SERVER['HOME'] ?? '.') . '/.faker-cli-history';
if (is_readable($historyFilePath)) {
    readline_read_history($historyFilePath);
}
$history = readline_list_history();
$previousCommand = count($history) > 0 ? array_pop($history) : null;
readline_completion_function([$fakerCompletion, 'complete']);

while (($command = readline('faker> ')) !== false) {
    if ($command) {
        if ($command !== $previousCommand) {
            readline_add_history($command);
        }
        $previousCommand = $command;
        $params = explode(' ', $command);
        $method = array_shift($params);
        try {
            $output = call_user_func_array([$faker, $method], $params);
        } catch (InvalidArgumentException $e) {
            echo $e->getMessage() . "\n";
            continue;
        }

        if (is_string($output) || is_numeric($output)) {
            echo "$output\n";
        } elseif ($output instanceof DateTime) {
            printf("%s\n", $output->format('Y-m-d H:i:s'));
        } elseif (is_array($output)) {
            print_r($output);
        }
    }
}

// If the history has more than MAX_HISTORY_SIZE entries, truncate before writing
$history = readline_list_history();
if (count($history) > MAX_HISTORY_SIZE) {
    readline_clear_history();
    $history = array_slice($history, -MAX_HISTORY_SIZE);
    foreach ($history as $historyEntry) {
        readline_add_history($historyEntry);
    }
}

readline_write_history($historyFilePath);