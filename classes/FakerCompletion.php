<?php
namespace FakerCli;

use Faker\Factory;
use Faker\Generator;
use \ReflectionClass;
use \ReflectionObject;
use \ReflectionMethod;

class FakerCompletion
{
    public $faker;
    public $methods = [];

    public function __construct(Generator $faker = null)
    {
        if ($faker instanceof Generator) {
            $this->faker = $faker;
        } else {
            $this->faker = Factory::create();
        }

        $this->initializeMethods();
    }

    public static function getMethodName(ReflectionMethod $method)
    {
        return $method->name;
    }

    public function initializeMethods()
    {
        $generatorClass = new ReflectionClass(Generator::class);
        $this->methods = array_map([static::class, 'getMethodName'], $generatorClass->getMethods(ReflectionMethod::IS_PUBLIC));

        foreach ($this->faker->getProviders() as $provider) {
            $providerReflection = new ReflectionObject($provider);
            $providerMethods = array_map([static::class, 'getMethodName'], $providerReflection->getMethods(ReflectionMethod::IS_PUBLIC));
            $this->methods = array_merge($this->methods, $providerMethods);
        }

        $this->methods = array_unique($this->methods);
        sort($this->methods);
    }

    public function complete(string $partialCommand)
    {
        if (empty($partialCommand)) {
            return $this->methods;
        } else {
            $partialLength = strlen($partialCommand);
            $matches = array_filter($this->methods, function(string $fullCommand) use ($partialCommand, $partialLength) {
                return substr($fullCommand, 0, $partialLength) == $partialCommand;
            });
            return $matches;
        }
    }
}